# sunlight-order

An awesome order management application.

# About this application

This is the (fullstack) boilerplate from AdonisJs, it has the following configured.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds
9. Swagger API documentation

## Using the system

Once the codes are cloned and run in the server/local environment. 
Visit this url to view the Swagger documentation of the API's
```
http://localhost:3333/docs/
```
All the API used in the system are protected routes with JSON Web Tokens (JWT). Either /register or /login and get the token `xxxxxxxxx`. Use this token as this in the header of each request.
`Bearer xxxxxxxx `

Now all the API's are ready to use.

Note: The console.log is specifically uncommented to view the status being changes for the order in the CLI. Feel fee to comment it out as required. 

I have a prepared few videos to do a quick demo on how the system works, make sure to check it out here. 

[`Running the Order server and Creating an Order`](https://www.mediafire.com/file/qp7tk74t1m63heo/1._Running_the_server_and_Create_order.mov/file)

[`Ordering with Payment system in place`](https://www.mediafire.com/file/3vfp11artyjiwld/2._Order_status_changing_from_payment_system.mov/file)

[`Running the Unit tests`](https://www.mediafire.com/file/2xgcxbuvmtr4zcm/3._Running_unit_tests.mov/file)

## Development
You will need the following

* NodeJs (version equals or large than v8.11.1)

### Set up
Follow these tasks to set up Sunlight-Orders locally for development/Testing:

#### 1. Get the code
Check out the sunlight-order code and navigate to the root directory:

`git@gitlab.com:pecsundar/sunlight-order.git`

#### 2. Build and start application

```
$ cd sunlight-order
$ npm i
$ npm run start  or  $ adonis serve
```

#### 3. Run migration
This will help to get all the required DB and tables in place.

```js
adonis migration:run
```


#### 4. Run lint

```
$ npm run eslint
```

#### 5. Run lint fix

```
$ npm run eslint:fix
```

#### 5. Run unit test


```
$ npm run test
```

#### 6. (Optional) If you want to Run by docker-compose

```
$ docker-compose -p order-api down \
    && docker-compose -p order-api build \
    && docker-compose -p order-api up api

```

or you can just run:

```
$ start_local.sh
```

