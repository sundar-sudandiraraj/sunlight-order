'use strict'
const Factory = use('Factory')
const { test } = use('Test/Suite')('Update Order')
const OrderController = use('App/Controllers/Http/OrderController')
const orderObj = new OrderController()

test('Able to Update an orders status by id', async ({ assert, client }) => {
  const order = await Factory.model('App/Models/Order').create()
  const isOrderUpdate = await orderObj.updateStatus(order.id, 'cancelled')
  assert.isTrue(isOrderUpdate)
})

test('unable to Update an orders status with invalid id', async ({ assert, client }) => {
  const order = await Factory.model('App/Models/Order').create()
  const isOrderUpdate = await orderObj.updateStatus(order.name, 'cancelled')
  assert.isFalse(isOrderUpdate)
})

test('unable to set an invalid status to an orders', async ({ assert, client }) => {
  const order = await Factory.model('App/Models/Order').create()
  const isOrderUpdate = await orderObj.updateStatus(order.id, 'approved')
  assert.isFalse(isOrderUpdate)
})
