'use strict'
const Factory = use('Factory')
const { test, trait } = use('Test/Suite')('Create Order')

trait('Test/ApiClient')
trait('Auth/Client')

test('cannot create a order if not authenticated', async ({ assert, client }) => {
  const { name, description } = await Factory.model('App/Models/Order').make()
  const data = { name, description }
  const response = await client
    .post('/api/v1/orders')
    .send(data)
    .end()
  response.assertStatus(401)
})

test('cannot create a order without name', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create()
  const { description } = await Factory.model('App/Models/Order').make()
  const data = { description }
  const response = await client
    .post('/api/v1/orders')
    .loginVia(user, 'jwt')
    .send(data)
    .end()
  response.assertStatus(400)
  response.assertJSONSubset([
    {
      message: 'name is required',
      field: 'name',
      validation: 'required'
    }
  ])
})

test('cannot create a order with name and description are not a string', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create()
  const data = { name: 123, description: 456 }
  const response = await client
    .post('/api/v1/orders')
    .loginVia(user, 'jwt')
    .send(data)
    .end()
  response.assertStatus(400)
  response.assertJSONSubset([
    {
      message: 'name is not a valid string',
      field: 'name',
      validation: 'string'
    },
    {
      message: 'description is not a valid string',
      field: 'description',
      validation: 'string'
    }
  ])
})

test('can create a order if authenticated and has valid data', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create()
  const { name, description } = await Factory.model('App/Models/Order').make()
  const data = { name, description }
  const response = await client
    .post('/api/v1/orders')
    .loginVia(user, 'jwt')
    .send(data)
    .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    message: 'Order Created Successfully'
  })
})
