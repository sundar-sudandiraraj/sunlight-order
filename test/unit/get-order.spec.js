'use strict'
const Factory = use('Factory')
const { test, trait } = use('Test/Suite')('Get Order')

trait('Test/ApiClient')
trait('Auth/Client')

test('can get all the orders', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create()
  const orders = await Factory.model('App/Models/Order').createMany(3)

  const response = await client.get('/api/v1/orders')
    .loginVia(user, 'jwt')
    .end()

  response.assertStatus(200)

  response.assertJSONSubset([
    { name: orders[0].name },
    { name: orders[1].name },
    { name: orders[2].name }
  ])
}).timeout(0)

test('can get an orders by id', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create()
  const orders = await Factory.model('App/Models/Order').createMany(3)
  const order = orders[0]

  const response = await client.get(`/api/v1/orders/${order.id}`)
    .loginVia(user, 'jwt')
    .end()

  response.assertStatus(200)
  response.assertJSONSubset(
    { name: order.name,
      description: order.description,
      id: order.id }
  )
})
