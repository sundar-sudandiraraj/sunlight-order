const Event = use('Event')
const sleep = use('sleep')
const axios = require('axios')
const OrderController = use('App/Controllers/Http/OrderController')
const orderObj = new OrderController()
const Config = use('Config')
const paymentEndpoint = Config.get('app.paymentapp.endpoint')

Event.on('new::order', async (order) => {
  console.log('Order id:' + order.id + ' is Created')
  let paymentStatus = ''
  try {
    const response = await axios.get(`${paymentEndpoint}` + '/api/v1/process-payment')
      .catch(error => {
        console.log(error.code)
      })
    paymentStatus = response.data.paymentStatus
    // paymentStatus = response.data.paymentStatus
  } catch (e) {
    console.log('Payment system is down. All Order with created status will be queued and sent to payment system via scheduled job later Or manually triggered via api')
  }
  if (paymentStatus === true) {
    console.log('Order id:' + order.id + ' Confirmed')
    if (await orderObj.updateStatus(order.id, 'confirmed')) {
      Event.fire('order::confirmed', order.id)
    }
  } else if (paymentStatus === false) {
    console.log('Order id:' + order.id + ' Cancelled')
    await orderObj.updateStatus(order.id, 'cancelled')
  }
})

Event.on('order::confirmed', async (orderId) => {
  await sleep.sleep(5)
  await orderObj.updateStatus(orderId, 'delivered')
  console.log('Order id:' + orderId + ' is Delivered')
})
