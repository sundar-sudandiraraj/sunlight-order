#!/usr/bin/env bash

docker-compose -p order-api down \
    && docker-compose -p order-api build \
    && docker-compose -p order-api up api
