'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Order = use('App/Models/Order')
const Event = use('Event')
const orderStatus = ['create', 'confirmed', 'cancelled', 'delivered']
/**
 * Resourceful controller for interacting with orders
 */
class OrderController {
  /**
   * Show a list of all orders.
   * GET orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  /**
 * @swagger
 * /orders:
 *   get:
 *     tags:
 *      - "Order"
 *     summary: View all orders
 *     description: "List all available order in the Application"
 *     produces:
 *       - application/json
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: All orders
 */
  async index ({ request, response, view }) {
    const orders = await Order.query().with('user').fetch()
    return response.json(orders)
  }

  /**
   * Create/save a new order.
   * POST orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
  * @swagger
  * /orders:
  *   post:
  *     tags:
  *      - "Order"
  *     summary: Create Order
  *     description: "Create Order"
  *     produces:
  *       - application/json
  *     security:
  *       - Bearer: []
  *     parameters:
  *       - name: name
  *         description: Order Name
  *         in: formData
  *         required: true
  *         type: string
  *       - name: description
  *         description: Order Description
  *         in: formData
  *         required: true
  *         type: string
  *     responses:
  *       200:
  *         description: Orders Created
  */
  async store ({ request, auth, response }) {
    try {
      // if (await auth.check()) {
      const order = await auth.user.orders().create(request.all())
      await order.load('user')
      const newOrder = await Order.find(order.id)
      if (newOrder.status === 'create') {
        await Event.fire('new::order', order)
      }
      const returnData = {
        message: 'Order Created Successfully',
        orderid: order.id
      }
      return response.status(201).json(returnData)
      // }
    } catch (e) {
      return response.status(400).json({ message: 'You are not authorized to perform this action' })
    }
  }

  /**
 * @swagger
 * /orders/{orderId}:
 *   get:
 *     tags:
 *      - "Order"
 *     summary: View Order Status
 *     description: Show the Order details
 *     produces:
 *       - application/json
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: orderId
  *         description: Id of the required Order
  *         in: path
  *         required: true
  *         type: integer
 *     responses:
 *       200:
 *         description: Current orders detail
 */
  async show ({ params, response }) {
    const order = await Order.find(params.id)
    return response.json(order)
  }

  /**
   * Render a form to update an existing order.
   * GET orders/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update order details.
   * PUT or PATCH orders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  /**
 * @swagger
 * /orders/{orderId}:
 *   put:
 *     tags:
 *      - "Order"
 *     summary: Update Order Status
 *     description: Update Order Status
 *     produces:
 *       - application/json
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: orderId
  *         description: Id of the required Order
  *         in: path
  *         required: true
  *         type: integer
  *       - name: status
  *         description: new Order Status
  *         in: formData
  *         required: true
  *         type: string
  *         enum: [cancelled, confirmed, delivered]
 *     responses:
 *       200:
 *         description: Current orders is updated
 */
  async update ({ params, request, response }) {
    const status = request.input('status')
    const orderId = params.id
    await this.updateStatus(orderId, status)
    if (status === 'confirmed') {
      console.log('Order id:' + orderId + ' Confirmed')
      Event.fire('order::confirmed', orderId)
    }
    return response.json({ message: 'Order status has been successfully changed' })
  }

  /**
   * Update Order status with id and status.
   *
   * @param {id} id
   * @param {status} status
   */
  async updateStatus (id, status) {
    try {
      if (orderStatus.includes(status)) {
        const order = await Order.find(id)
        order.status = status
        await order.save()
        return true
      } else {
        return false
      }
    } catch (e) {
      return false
    }
  }
}

module.exports = OrderController
